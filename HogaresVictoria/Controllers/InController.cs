﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HogaresVictoria.Models;

namespace HogaresVictoria.Controllers
{
    public class InController : Controller
    {
        private HogaresVictoriaEntities DB = new HogaresVictoriaEntities();

        // GET: Inicio
        public ActionResult Index()
        {
            var Lastest = DB.Hogares.OrderByDescending(x => x.ID).Take(5).ToList();
            return View(Lastest);
        }

        public ActionResult Contacto()
        {
            return View(DB.Contacto.ToList());
        }

        public ActionResult Casas() {
            var datos = DB.Hogares.Where(x => x.IDTipo == 1).ToList();
            return View(datos);
        }

        //get locales en renta from db
        public ActionResult Locales()
        {
            return View(DB.Hogares.Where(x => x.IDTipo == 2).ToList());
        }
        
        //Get terrenos from db
        public ActionResult Terrenos()
        {
            return View(DB.Hogares.Where(x => x.IDTipo == 3).ToList());
        }

    }
}